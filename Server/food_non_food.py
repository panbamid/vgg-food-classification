import os

# parent directory of dataset
os.chdir("")

import tensorflow as tf 

# uncomment if you run on GPU
# config = tf.ConfigProto()
# config.gpu_options.allow_growth = True
# sess = tf.Session(config=config)

from keras.applications.vgg16 import VGG16
from keras.models import load_model
from keras import models
from keras import optimizers
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D, Flatten, Dropout, Conv2D, MaxPooling2D
from keras import optimizers
from keras.callbacks import CSVLogger, ModelCheckpoint

base_model = VGG16(weights = 'imagenet', include_top = False, input_shape=(224, 224, 3))

for layer in base_model.layers:
   layer.trainable = False

for layer in base_model.layers:
   print(layer, layer.trainable)

x = base_model.output
x = Flatten()(x)
x = Dropout(0.5)(x)
x = Dense(4096, activation = 'relu')(x)
x = Dropout(0.5)(x)
x = Dense(4096, activation = 'relu')(x)
x = Dropout(0.5)(x)

predictions = Dense(1, activation='sigmoid')(x)
model = Model(inputs=base_model.input, outputs=predictions)

model.compile(loss = "binary_crossentropy", optimizer = optimizers.SGD(lr = 1e-4, momentum = 0.9), metrics = ['accuracy'])
model.summary()

# train the model on the new data for a few epochs
from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(
        rescale = 1./255,
        rotation_range = 45,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        )

test_datagen = ImageDataGenerator(rescale = 1./255)


training_set = train_datagen.flow_from_directory('Binary/train',
                                                 target_size=(224, 224),
                                                 batch_size=8,
                                                 class_mode='binary')

test_set = test_datagen.flow_from_directory('Binary/val',
                                            target_size=(224, 224),
                                            batch_size=8,
                                            class_mode='binary')

csvLogger = CSVLogger('training.log')
filepath="Epoch-{epoch:02d}-{loss:.2f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=False)
history = model.fit_generator(training_set,        
                    steps_per_epoch=32000/8,
                    epochs=100,
                    validation_data=test_set,
                    validation_steps=8000/8,
                    callbacks = [csvLogger, checkpoint])