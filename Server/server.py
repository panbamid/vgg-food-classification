from flask import Flask
from flask import request
from flask import Response
from flask import jsonify, make_response, json
import tensorflow as tf
from keras.models import load_model
from keras.preprocessing import image
from keras import backend as K
import numpy as np 
import io
import os
import localization
import cv2

from PIL import Image
import base64

global graph, graph2, model_binary, model_class
graph = tf.get_default_graph()
model_binary = load_model('model_binary.hdf5')
graph2 = tf.get_default_graph()
model_class = load_model('model_class.hdf5')
graph3 = tf.get_default_graph()

app = Flask(__name__)
print('The application has been initialized')

@app.route("/test", methods=['POST'])
def parse_request():
    print('message received')
    data = request.get_json().get("Image")
    imgdata = base64.b64decode(data)

    fh = open('sent_image.jpg', 'wb')
    fh.write(imgdata)
    fh.close()

    labels_path = 'classes.txt'

    img = image.load_img('sent_image.jpg', target_size=(224, 224))
    img_tensor = image_preprocess(img)

    with graph3.as_default():
        cropped_image = localize()
    
    with graph.as_default():
        foodNonFood = model_binary.predict(img_tensor)
        print(foodNonFood) 
       
    if foodNonFood <= 0.5:
       
       # if food is identified, continue with next step
        cropped_image_tensor = image_preprocess(cropped_image)
        labels = localization.get_classes(labels_path)

        with graph2.as_default():
            classes = model_class.predict(img_tensor)
            cropped_classes = model_class.predict(cropped_image_tensor)
            prob = classes[0][np.argmax(classes)]
            cropped_prob = cropped_classes[0][np.argmax(cropped_classes)]
            
            # pick the decision with the highest probability
            if prob >= cropped_prob:
                label = str(labels[np.argmax(classes)])
                probability = str(prob*100)
            else:
                label = str(labels[np.argmax(cropped_classes)])
                probability = str(cropped_prob*100)
            
            data = {
                'message': '',
                'label': label,
                'probability': probability
                }
            js = json.dumps(data)
            resp = Response(js, status=200, mimetype='application/json')
            return resp    

    else:
        data = {'message': 'You did not provide a food image'}
        js = json.dumps(data)
        resp = Response(js, status=200, mimetype='application/json')
        return resp

def image_preprocess(img):
    img_tensor = image.img_to_array(img)
    img_tensor = np.expand_dims(img_tensor, axis=0)  
    img_tensor /= 255.
    return img_tensor

def localize():
    cropped_image = localization.localization('sent_image.jpg')
    cv2.imwrite('cropped.jpg', cropped_image)

    cropped_img = image.load_img('cropped.jpg', target_size=(224, 224))
    return cropped_img


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

# run the following commands to run the flask server:

# cd <file_directory>
# set FLASK_ENV=development
# set FLASK_APP=server.py
# flask run --without-threads