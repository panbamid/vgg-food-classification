import tensorflow as tf 

# uncomment if you run on GPU
# config = tf.ConfigProto()
# config.gpu_options.allow_growth = True
# sess = tf.Session(config=config)

import os

# parent directory of dataset
os.chdir("")

from keras.applications.vgg16 import VGG16
from keras.models import load_model
from keras import models
from keras import optimizers
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D, Flatten, Dropout, Conv2D, MaxPooling2D
from keras import optimizers
from keras.callbacks import CSVLogger, ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator


# Generate a model with all layers (with top)
vgg16 = VGG16(weights='imagenet', include_top=True)

#Add a layer where input is the output of the  second last layer 
x = Dense(101, activation='softmax', name='predictions')(vgg16.layers[-2].output)

#Then create the corresponding model 
my_model = Model(input=vgg16.input, output=x)
my_model.summary()

for layer in my_model.layers[:-1]:
   layer.trainable = False

for layer in my_model.layers:
   print(layer, layer.trainable)



my_model.compile(loss = "categorical_crossentropy", optimizer = optimizers.sgd(lr = 1e-4, momentum = 0.9), metrics = ['accuracy'])

train_datagen = ImageDataGenerator(
    rescale = 1./255,
    rotation_range = 40,
    width_shift_range = 0.2,
    height_shift_range = 0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode = 'nearest'
    )

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory('dataset_101/train',
                                                 target_size=(224, 224),
                                                 batch_size=32,
                                                 class_mode='categorical')

test_set = test_datagen.flow_from_directory('dataset_101/val',
                                            target_size=(224, 224),
                                            batch_size=32,
                                            class_mode='categorical')

csvLogger = CSVLogger('training.log');
filepath="Epoch-{epoch:02d}-{loss:.2f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=False)
history = my_model.fit_generator(training_set,        
                    steps_per_epoch=80800/32,
                    epochs=200,
                    validation_data=test_set,
                    validation_steps=20200/32,
                    callbacks = [csvLogger, checkpoint])